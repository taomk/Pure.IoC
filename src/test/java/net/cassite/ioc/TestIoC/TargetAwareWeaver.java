package net.cassite.ioc.TestIoC;

import net.cassite.pure.aop.AOPPoint;
import net.cassite.pure.aop.TargetAware;
import net.cassite.pure.aop.LoggedWeaver;

public class TargetAwareWeaver extends LoggedWeaver implements TargetAware<Object> {
        private Object target;

        @Override
        public void before(AOPPoint point) {
                assert target != null;
        }

        @Override
        public void after(AOPPoint point) {

        }

        @Override
        public void exception(AOPPoint point) throws Throwable {

        }

        @Override
        public void targetAware(Object o) {
                this.target = o;
        }
}
