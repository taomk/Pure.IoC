package net.cassite.ioc.TestIoC;

import net.cassite.pure.aop.AOPPoint;
import net.cassite.pure.aop.LoggedWeaver;

public class ChangeArgBeforeInvokingWeaver extends LoggedWeaver {
        @Override
        public void before(AOPPoint point) {
                if (point.method.getName().equals("setName")) {
                        point.args[0] = point.args[0] + "|aop";
                }
        }

        @Override
        public void after(AOPPoint point) {

        }

        @Override
        public void exception(AOPPoint point) throws Throwable {

        }
}
