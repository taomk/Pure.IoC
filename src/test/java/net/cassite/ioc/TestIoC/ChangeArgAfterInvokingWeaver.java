package net.cassite.ioc.TestIoC;

import net.cassite.pure.aop.AOPPoint;
import net.cassite.pure.aop.LoggedWeaver;

public class ChangeArgAfterInvokingWeaver extends LoggedWeaver {
        @Override
        public void before(AOPPoint point) {

        }

        @Override
        public void after(AOPPoint point) {
                if (point.method.getName().equals("getName")) {
                        point.returnValue(point.returnValue() + "|aop");
                }
        }

        @Override
        public void exception(AOPPoint point) throws Throwable {

        }
}
