package net.cassite.ioc.TestIoC;

import net.cassite.pure.ioc.AutoWire;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class TestBindBean extends AutoWire implements ApplicationContextAware {
        private ApplicationContext context;

        @Override
        public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
                this.context = applicationContext;
        }

        public ApplicationContext getContext() {
                return context;
        }
}
