package net.cassite.ioc.TestIoC;

import net.cassite.pure.ioc.AutoWire;
import net.cassite.pure.ioc.IOCController;
import net.cassite.pure.ioc.Scope;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.Properties;

import static org.junit.Assert.*;

/**
 * test ioc
 */
public class TestIoC {
        private static ApplicationContext ctx;

        @BeforeClass
        public static void classSetUp() throws IOException {
                // IOCController.autoRegister();
                IOCController.rootScope.registerInstance("testConst", new TestIoC());
                IOCController.rootScope.register("testVar", scope -> AutoWire.get(BeanFactory.class).getBean());
                IOCController.rootScope.register("testVar2", scope -> BeanFactory.text);

                ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
                IOCController.rootScope.bind(ApplicationContext.class, session -> ctx);

                Properties properties = new Properties();
                properties.load(TestIoC.class.getResourceAsStream("/test.properties"));
                IOCController.rootScope.registerProperties("prop", properties);
                // IOCController.closeRegistering();
        }

        @Test
        public void testAutoWire() {
                BeanA a = new BeanA();
                assertNotNull(a.getB());
        }

        @Test
        public void testSingleton() {
                BeanB b1 = AutoWire.get(BeanB.class);
                BeanB b2 = AutoWire.get(BeanB.class);
                assertTrue(b1.getC() == b2.getC());
        }

        @Test
        public void testWire() {
                BeanB b = new BeanB(1.0);
                assertNull(b.getC());
                BeanB b2 = AutoWire.get(BeanB.class);
                assertNotNull(b2.getC());
        }

        @Test
        public void testCircularDep() {
                BeanA a = new BeanA();
                try {
                        a.getB().getC().getA().getB().getC().getA();
                        assertTrue(a.getB().getC() == a.getB().getC().getA().getB().getC());
                } catch (NullPointerException e) {
                        fail();
                }
        }

        @Test
        public void testTypeDefault() {
                BeanC c = AutoWire.get(BeanC.class);
                assertNotNull(c.getD());
                assertTrue(c.getD() instanceof BeanD);
        }

        @Test
        public void testConstructorDefault() {
                BeanC c = AutoWire.get(BeanC.class);
                assertEquals(1, c.getI());
        }

        @Test
        public void testSetterForce() {
                BeanA a = new BeanA();
                assertEquals("x", a.getX());
        }

        @Test
        public void testParamForce() {
                BeanB b = AutoWire.get(BeanB.class);
                assertEquals(2.0, b.getD(), 0);
        }

        @Test
        public void testIgnore() {
                BeanA a = AutoWire.get(BeanA.class);
                assertNull(a.getD());
        }

        @Test
        public void testUse() {
                BeanFactory.text = "text1";
                BeanD d1 = new BeanD();
                BeanFactory.text = "text2";
                BeanD d2 = new BeanD();

                assertEquals("text1", d1.getText());
                assertEquals("text2", d2.getText());

                assertNotNull(d1.getA());
                assertNotNull(d2.getA());
                assertNull(d1.getA().getB());
                assertNull(d2.getA().getB());
                assertTrue(d1.getTestIoC() == d2.getTestIoC());
        }

        @Test
        public void testReturnBeforeInvokingAOP() {
                ReturnBeforeInvokingAOPBean bean = AutoWire.get(ReturnBeforeInvokingAOPBean.class);
                assertEquals("doAop", bean.getName());
                bean.setName("123");
                assertEquals("doAop", bean.getName());
        }

        @Test
        public void testChangeArgBeforeInvokingAOP() {
                ChangeArgBeforeInvokingAOPBean bean = AutoWire.get(ChangeArgBeforeInvokingAOPBean.class);
                bean.setName("test");
                assertEquals("test|aop", bean.getName());
        }

        @Test
        public void testChangeReturnAfterInvokingAOP() {
                ChangeReturnAfterInvokingAOPBean bean = AutoWire.get(ChangeReturnAfterInvokingAOPBean.class);
                bean.setName("test");
                assertEquals("test|aop", bean.getName());
        }

        @Test
        public void testThrowAOP() {
                ThrowAOPBean bean = AutoWire.get(ThrowAOPBean.class);
                bean.setName("test");
                assertEquals("doAop", bean.getName());
        }

        @Test
        public void testAllAOP() {
                AllAOPBean bean = AutoWire.get(AllAOPBean.class);
                bean.setName("test");
                assertEquals("test|aop|aop", bean.getName());
                assertEquals("doAop", bean.name());
        }

        @Test
        public void testTargetAware() {
                TargetAwareAOPBean bean = AutoWire.get(TargetAwareAOPBean.class);
                bean.doSomething();
        }

        @Test
        public void testSession() {
                Bean1 bean1 = AutoWire.get(Bean1.class);
                assertTrue(bean1.getBeanA() == bean1.getBean2().getBeanA());
                assertTrue(bean1.getBeanA() == bean1.getBean3().getBeanA());
        }

        @Test
        public void testBind() {
                TestBindBean bean = new TestBindBean();
                assertNotNull(bean.getContext());
                assertNotNull(bean.getContext().getBean("springTestBean"));

                assertEquals(ctx.getBean("springTestBean"), bean.getContext().getBean("springTestBean"));
        }

        @Test
        public void testExtendAndSpring() {
                SpringExtTestBean bean = AutoWire.get(SpringExtTestBean.class);
                assertTrue(ctx.getBean("springTestBean") == bean.getBean());
                assertTrue(bean.getBean() == bean.getBean2());
                assertTrue(bean.getBean() == bean.getBean3());
        }

        @Test
        public void testForceProperty() {
                TestForceProperty bean = new TestForceProperty();
                assertEquals(1, bean.getA());
                assertEquals("b", bean.getB());
                assertEquals('c', bean.getC());
        }

        @Test
        public void testPrimitive() {
                PrimitiveBean bean = AutoWire.get(PrimitiveBean.class);
                assertEquals(0, bean.getI());
                assertEquals(0, bean.getD(), 0);
        }

        @Test
        public void testFieldInjection() {
                FieldInjectBean bean = new FieldInjectBean();
                assertEquals(1, bean.integer);
                assertEquals("a", bean.string);
        }

        @Test
        public void testNonClassWire() {
                NonClassWire bean = AutoWire.get(NonClassWire.class);
                assertEquals(1, bean.getInteger());
                assertEquals("a", bean.string);
        }

        @Test
        public void testScopeAware() {
                ScopeAwareBean bean = AutoWire.get(ScopeAwareBean.class);
                assertNotNull(bean.getScope());
                assertNotNull(bean.getScope().getBondInstance(Scope.class));
        }
}
