package net.cassite.ioc.TestIoC;

import net.cassite.pure.aop.AOPPoint;
import net.cassite.pure.aop.LoggedWeaver;

public class ReturnBeforeInvokingWeaver extends LoggedWeaver {
        @Override
        public void before(AOPPoint point) {
                if (point.method.getName().equals("getName")) {
                        point.returnValue("doAop");
                }
        }

        @Override
        public void after(AOPPoint point) {

        }

        @Override
        public void exception(AOPPoint point) throws Throwable {

        }
}
