package net.cassite.ioc.TestIoC;

import net.cassite.pure.ioc.annotations.Extend;
import net.cassite.pure.ioc.ext.SpringExt;

@Extend(handler = SpringExt.class, args = "springTestBean")
public class SpringTestBean {
}
