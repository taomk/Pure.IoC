package net.cassite.ioc.TestIoC;

import net.cassite.pure.ioc.annotations.Scope;
import net.cassite.pure.ioc.annotations.Wire;

@Wire
public class Bean3 {
        private BeanA beanA;

        public BeanA getBeanA() {
                return beanA;
        }

        public void setBeanA(@Scope("beanA") BeanA beanA) {
                this.beanA = beanA;
        }
}
