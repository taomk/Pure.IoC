package net.cassite.ioc.TestIoC;

import net.cassite.pure.aop.AOPPoint;
import net.cassite.pure.aop.LoggedWeaver;

public class ThrowWeaver extends LoggedWeaver {
        @Override
        public void before(AOPPoint point) {

        }

        @Override
        public void after(AOPPoint point) {

        }

        @Override
        public void exception(AOPPoint point) throws Throwable {
                point.returnValue("doAop");
        }
}
