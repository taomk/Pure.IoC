package net.cassite.pure.ioc.annotations;

import java.lang.annotation.*;

/**
 * indicate that the instance will be shared in one construction process
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Scope {
        /**
         * name of registered instance in scope
         *
         * @return name
         */
        String value();
}
