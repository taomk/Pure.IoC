package net.cassite.pure.ioc.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TypeHandlerChain {

        private static final Logger LOGGER = LoggerFactory.getLogger(TypeHandlerChain.class);

        private final Iterator<TypeAnnotationHandler> it;

        public TypeHandlerChain(List<TypeAnnotationHandler> handlers, Annotation[] anns) {

                List<TypeAnnotationHandler> list = new ArrayList<>();
                handlers.forEach(h -> {
                        if (h.canHandle(anns)) {
                                list.add(h);
                        }
                });
                list.add(EmptyHandler.getInstance());

                LOGGER.debug("Generate Type Chain with handlers {}", list);

                it = list.iterator();
        }

        public TypeAnnotationHandler next() {
                return it.next();
        }
}
