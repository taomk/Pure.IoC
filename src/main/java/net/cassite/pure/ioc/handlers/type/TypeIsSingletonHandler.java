package net.cassite.pure.ioc.handlers.type;

import java.lang.annotation.Annotation;

import net.cassite.pure.ioc.AnnotationHandlingException;
import net.cassite.pure.ioc.IOCController;
import net.cassite.pure.ioc.Scope;
import net.cassite.pure.ioc.annotations.Singleton;
import net.cassite.pure.ioc.handlers.TypeAnnotationHandler;
import net.cassite.pure.ioc.handlers.TypeHandlerChain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handler for IsSingleton annotation. <br>
 * the class would be considered as a singleton.
 *
 * @author wkgcass
 * @see net.cassite.pure.ioc.annotations.Singleton
 */
public class TypeIsSingletonHandler extends IOCController implements TypeAnnotationHandler {

        private static final Logger LOGGER = LoggerFactory.getLogger(TypeIsSingletonHandler.class);

        @Override
        public boolean canHandle(Annotation[] annotations) {
                for (Annotation ann : annotations) {
                        if (ann.annotationType() == Singleton.class) {
                                return true;
                        }
                }
                return false;
        }

        @Override
        public Object handle(Scope scope, Class<?> cls, TypeHandlerChain chain) throws AnnotationHandlingException {
                LOGGER.debug("Entered TypeIsSingletonHandler with args: \n\tcls:\t{}\n\tchain:\t{}", cls, chain);
                try {
                        return chain.next().handle(scope, cls, chain);
                } catch (AnnotationHandlingException e) {
                        LOGGER.debug("start handling with TypeIsSingletonHandler");
                        return getObject(scope, cls);
                }
        }

}
