package net.cassite.pure.ioc.handlers.param;

import net.cassite.pure.ioc.AnnotationHandlingException;
import net.cassite.pure.ioc.Utils;
import net.cassite.pure.ioc.annotations.Scope;
import net.cassite.pure.ioc.handlers.ParamAnnotationHandler;
import net.cassite.pure.ioc.handlers.ParamHandlerChain;
import net.cassite.style.reflect.MemberSup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;

/**
 * handler for annotation @Scope on parameters
 */
public class ParamScopeHandler implements ParamAnnotationHandler {
        private static final Logger LOGGER = LoggerFactory.getLogger(ParamScopeHandler.class);

        @Override
        public boolean canHandle(Annotation[] annotations) {
                return null != Utils.getAnno(Scope.class, annotations);
        }

        @SuppressWarnings("unchecked")
        @Override
        public Object handle(net.cassite.pure.ioc.Scope scope, MemberSup<?> caller, Class<?> cls, Annotation[] toHandle, ParamHandlerChain chain) throws AnnotationHandlingException {
                LOGGER.debug("Entered ParamScopeHandler with args:\n\tcaller:\t{}\n\tcls:\t{}\n\ttoHandle:\t{}\n\tchain:\t{}",
                        caller, cls, toHandle, chain);
                LOGGER.debug("Start handling with ParamScopeHandler");

                Scope scopeA = Utils.getAnno(Scope.class, toHandle);
                assert scopeA != null;

                if (scope.containsKey(scopeA.value())) {
                        return scope.get(scopeA.value());
                } else {
                        Object retrieved = chain.next().handle(scope, caller, cls, toHandle, chain);
                        scope.registerInstance(scopeA.value(), retrieved);
                        return retrieved;
                }
        }
}
