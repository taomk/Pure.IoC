package net.cassite.pure.aop;

/**
 * interface for weavers
 *
 * @since 0.2.1
 */
public interface Weaver {
        /**
         * before invoking the method
         *
         * @param point aop point
         */
        void doBefore(AOPPoint point);

        /**
         * after the method returned
         *
         * @param point aop point
         */
        void doAfter(AOPPoint point);

        /**
         * after throwing
         *
         * @param point aop point
         * @throws Throwable possible exceptions
         */
        void doException(AOPPoint point) throws Throwable;
}
