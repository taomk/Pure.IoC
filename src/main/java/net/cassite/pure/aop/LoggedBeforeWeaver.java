package net.cassite.pure.aop;

/**
 * Before
 *
 * @author wkgcass
 * @since 0.1.1
 */
public abstract class LoggedBeforeWeaver extends LoggedWeaver {
        @Override
        public final void exception(AOPPoint point) throws Throwable {
                throw point.exception();
        }

        @Override
        public final void after(AOPPoint point) {
        }
}
