package net.cassite.pure.aop;

/**
 * After Return
 *
 * @author wkgcass
 * @since 0.1.1
 */
public abstract class LoggedAfterWeaver extends LoggedWeaver {
        @Override
        public final void before(AOPPoint point) {
        }

        @Override
        public final void exception(AOPPoint point) throws Throwable {
                throw point.exception();
        }
}
