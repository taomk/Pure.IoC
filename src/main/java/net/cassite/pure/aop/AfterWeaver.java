package net.cassite.pure.aop;

/**
 * weaver only concerns AfterReturn
 */
public interface AfterWeaver extends Weaver {
        @Override
        default void doBefore(AOPPoint point) {
        }

        @Override
        default void doException(AOPPoint point) throws Throwable {
                throw point.exception();
        }
}
