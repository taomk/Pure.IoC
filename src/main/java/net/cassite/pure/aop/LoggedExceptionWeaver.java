package net.cassite.pure.aop;

/**
 * After Throwing
 *
 * @author wkgcass
 * @since 0.1.1
 */
public abstract class LoggedExceptionWeaver extends LoggedWeaver {
        @Override
        public final void before(AOPPoint point) {
        }

        @Override
        public final void after(AOPPoint point) {
        }
}
