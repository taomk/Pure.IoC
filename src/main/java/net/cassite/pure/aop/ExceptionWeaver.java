package net.cassite.pure.aop;

/**
 * weaver only concerns AfterThrown
 */
public interface ExceptionWeaver extends Weaver {
        @Override
        default void doBefore(AOPPoint point) {
        }

        @Override
        default void doAfter(AOPPoint point) {
        }
}
