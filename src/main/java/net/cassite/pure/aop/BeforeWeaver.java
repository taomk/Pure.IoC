package net.cassite.pure.aop;

import net.cassite.pure.aop.AOPPoint;
import net.cassite.pure.aop.Weaver;

/**
 * weaver only concerns before invoke
 */
public interface BeforeWeaver extends Weaver {
        @Override
        default void doAfter(AOPPoint point) {
        }

        @Override
        default void doException(AOPPoint point) throws Throwable {
                throw point.exception();
        }
}
