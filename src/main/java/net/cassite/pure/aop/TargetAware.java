package net.cassite.pure.aop;

/**
 * suggest that the weaver is aware of it's target
 *
 * @param <Target> target type
 * @author wkgcass
 * @since 0.2.1
 */
public interface TargetAware<Target> {
        /**
         * set proxy target to the weaver<br>
         * The implementation should record the argument as a field
         *
         * @param target proxy target
         */
        void targetAware(final Target target);
}
